package com.company;


import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class UniqueString {
    Map<String, Long> wordsCount(String line) {
        Pattern word = Pattern.compile("\\w+", Pattern.UNICODE_CHARACTER_CLASS);
        Matcher matcher = word.matcher(line);
        Map<String, Long> wordCount = new HashMap<>();
        while (matcher.find()) {
            String words = matcher.group().toLowerCase();
            wordCount.merge(words, 1L, Long::sum);
        }
        return wordCount;
    }

    void pick(Reader reader) {
        System.out.println(wordsCount(reader.getStrings().toString()));


    }



}
