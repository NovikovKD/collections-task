package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;


public class Reader {
    List<String> strings = new ArrayList<>();
    File file = new File("file.txt");
    Scanner input = new Scanner(file);
    {
        while (input.hasNext()) {
            String word = input.next();
            strings.add(word);
        }
        System.out.println(strings.size());
        strings.sort(Comparator.comparing(String::toString));
        System.out.println(strings);

    }



    public List<String> getStrings() {
        return strings;
    }

    public void setStrings(List<String> strings) {
        this.strings = strings;
    }

    public Scanner getInput() {
        return input;
    }

    public void setInput(Scanner input) {
        this.input = input;
    }

    public File getFile() {

        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Reader() throws FileNotFoundException {
    }
}





